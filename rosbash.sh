#! /bin/bash
# starts new bash shell in which ROS is connected to hog3's docker container

network="hog3_network"

# check if project has been built
if ! docker network ls | grep -q $network; then
	echo "Network not found: $network"
	echo 'Try: ./setup.sh'
	exit -1
fi

# get the IPs
HOST_ROS_IP=$(docker network inspect --format='{{range .IPAM.Config}}{{.Gateway}}{{end}}' $network)
DOCKER_ROS_IP=$(cat .env | sed -r "s/.*=([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/\1/g")

# set up environmental variables
export ROS_IP="$HOST_ROS_IP"
export ROS_MASTER_URI="http://$DOCKER_ROS_IP:11311"

# create custom command prompt
oldPrompt=$(bash -i -c 'echo $PS1')
newPrompt="@hog3 $oldPrompt "

# start bash with variables set up
bash --rcfile <(cat $HOME/.bashrc; echo "PS1=\"$newPrompt\"")
