#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>
#include "../inc/sterowanie.hh"
#include <hog3/ServoAngles.h>
/*!
   \file MovementControl.cpp
   \brief W pliku znajduje się wezeł odpowiedzialny za obiczenie wychylenia
   poszczególnych serw na podstawie zadanego wektora translacji oraz rotacji.
   \author Dawid Marszałkiewicz
*/

/**
 * \brief Klasa obliczająca wychylenia poszczególnych serw
 * Wezel MovementControl, który oblicza kąty poszczególnych serw na podstawie
 * żądania znajdującego się w wiadomości Twist:\n
 * - linear.x: prędkośc po osi X,\n
 * - linear.y: prędkośc po osi Y,\n
 * - angular.z: prędkość kątowa osi Z.
 */
class MovementControl
{
public:
  MovementControl();

private:
  /**
   * \brief Uchwyt do węzła
   */
  ros::NodeHandle uchwytDoWezla;
  /**
   * \brief Subskrybent
   */
  ros::Subscriber subTwist;
  /**
   * \brief Publicysta
   */
  ros::Publisher pubAngles;

  /**
   * \brief Tablica z katami wychylen poszczegolnych serw
   */
  angle serv[6];
  /**
   * \brief Zadany wektor prędkości liniowej
   */
  Vect2D Vin;
  /**
   * Zadana prędkość kątowa
   */
  angle omega_zadana;

  void odebranoWiadomosc(const geometry_msgs::Twist::ConstPtr& twist);
  void przypiszKatyDoWiadomosci(hog3::ServoAngles& katy, const angle *serwa);
};

/**
 * W konstruktorze tworzony jest susbskrybentai publicysta.
 */
MovementControl::MovementControl()
{
  subTwist = uchwytDoWezla.subscribe<geometry_msgs::Twist>\
            ("geometry_msgs/Twist", 10, &MovementControl::odebranoWiadomosc, this);
  pubAngles = uchwytDoWezla.advertise<hog3::ServoAngles>("hog3/ServoAngles", 1);
}

/**
 * Funckja typu Callback, która zostaje wywołana przez susbskrybenta
 * po odebraniu wiadomosći typu Twist. Ta wiadomość zostaje przekształcona
 * na wiadomość ServoAngles i wysłana przez publicyste.
 *
 * @param twist Odebrana wiadomość
 */
void MovementControl::odebranoWiadomosc(const geometry_msgs::Twist::ConstPtr& twist)
{
  hog3::ServoAngles katy;
  Vin.x[0] = twist->linear.x;
  Vin.x[1] = twist->linear.y;
  omega_zadana = twist->angular.z;
  ABCstep(serv, Vin, omega_zadana);
  przypiszKatyDoWiadomosci(katy, serv);
  pubAngles.publish(katy);
}

/**
 * Przypisanie katow obliczonych przez ABCstep, które znajdują sie w tablicy serwa
 * do wiadomosci katy.
 *
 * @param katy Wiadomość z kątami, która ma zostać wysłana
 * @param serwa Obliczona tablica wychyleń  
 */
void MovementControl::przypiszKatyDoWiadomosci(hog3::ServoAngles& katy, const angle *serwa)
{
  katy.left.external = serwa[0];
  katy.left.internal = serwa[1];
  katy.front.external = serwa[2];
  katy.front.internal = serwa[3];
  katy.right.external = serwa[4];
  katy.right.internal = serwa[5];
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "MovementControl");
  MovementControl sterownik;
  ros::spin();
}
