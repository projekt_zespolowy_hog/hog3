#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <hog3/ServoAngles.h>

/*!
   \file VrepLink.hh
   \brief W pliku znajduje się klasa modelująca łącznika do V-REPa, jest on
   niezbędny, ponieważ tłumaczy wiadomość hog3/ServoAngles na wbudowaną
   wiadomość geometry_msgs/Twist
   \author Dawid Marszałkiewicz
*/

/**
 * \brief Klasa VrepLink modeluje łącze do V-REPa
 */

class VrepLink
{
public:
/**
 * \brief Konstruktor
 */
  VrepLink();

private:
  /**
   * \brief Uchwyt do tworzonego węzła VrepLink
   */
  ros::NodeHandle uchwytDoWezla;
  /**
   * \brief Subskrybent, który odbiera wiadomości typy hog3/ServoAngles
   */
  ros::Subscriber subServoAngles;
  /**
   * \brief Publicysta, który wysyła wiadomości typu geometry_msgs/Twist
   */
  ros::Publisher pubTwist;
  /**
   * \brief Callback od odebrania wiadomości
   */
  void servoAngles_callback(const hog3::ServoAngles::ConstPtr& katy);
};


/**
 * Funkcja typu callback, która odebrane katy z wiadomośći hog3/ServoAngles
 * zamienia na wiadomość typu geometry_msgs/Twist i publikuje je w ramach tematu
 * hog3/Twist. W tej funkcji nie zmieniane są wartości kątów. Następuje tylko
 * ich przepisanie
 * @param katy Kąty wychylenia serw w radianach
 */
void VrepLink::servoAngles_callback(const hog3::ServoAngles::ConstPtr& katy) {
	  geometry_msgs::Twist twist;
	  twist.linear.x = katy->left.external;
	  twist.linear.y = katy->left.internal;
	  twist.linear.z = katy->front.external;
	  twist.angular.x = katy->front.internal;
	  twist.angular.y = katy->right.external;
	  twist.angular.z = katy->right.internal;
	  pubTwist.publish(twist);
  }
/**
 * W konstruktorze następuje inicjalizacja publicysty i subskrybenta.
 */
VrepLink::VrepLink() {
	  subServoAngles = uchwytDoWezla.subscribe<hog3::ServoAngles>\
				("hog3/ServoAngles", 10, &VrepLink::servoAngles_callback, this);
	  pubTwist = uchwytDoWezla.advertise<geometry_msgs::Twist>("hog3/Twist", 1);
  }


int main(int argc, char** argv)
{
  ros::init(argc,argv, "VrepLink");
  VrepLink vrepLink;

  ros::spin();
}
