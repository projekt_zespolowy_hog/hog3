#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>

/*!
 * \file JoyInterpreter.cpp
 * \brief Przerobiony przykład z tutoriala:
 * http://wiki.ros.org/joy/Tutorials/WritingTeleopNode
 * \author Dawid Marszałkiewicz
 */

/**
 * \brief Klasa JoyInterpreter odpowiada za odbieranie danych z Joysticka,
 * tłumaczenie na zadany kierunek ruchu oraz wysyłanie wiadomości
 * geometry_msgs/Twist
 */
class JoyInterpreter
{
public:
  /**
   * \brief Konstruktor
   */
  JoyInterpreter();

private:
  /**
   * \brief Uchwyt do węzła
   */
  ros::NodeHandle uchwytDoWezla;


  int osTranslacjiX, osTranslacjiY, osRotacjiZ;
  double maxX, maxY, maxZ;
  ros::Subscriber subJoy;
  ros::Publisher pubCmdVelocity;

  /**
   * \brief Callback od odebrania wiadomosći
   */
  void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);
};

/**
 * W konstruktorze dokonywany jest wybór osi Joysticka zgodnie z plikiem launch.
 * Jeżeli w pliku nie zaznaczono inaczej to domyślne wartości sa podane w liście
 * inicjalizacyjnej. Dodatkowo następuje zainicjalizowanie publicysty i
 * subskrybenta.
 */
JoyInterpreter::JoyInterpreter(): osTranslacjiX(1), osTranslacjiY(2), osRotacjiZ(3)
{
  uchwytDoWezla.param("axis_trans_X", osTranslacjiX, osTranslacjiX);
  uchwytDoWezla.param("axis_trans_Y", osTranslacjiY, osTranslacjiY);
  uchwytDoWezla.param("axis_rot_Z", osRotacjiZ, osRotacjiZ);
  uchwytDoWezla.param("scale_X", maxX, maxX);
  uchwytDoWezla.param("scale_Y", maxY, maxY);
  uchwytDoWezla.param("scale_Z", maxZ, maxZ);

  pubCmdVelocity = uchwytDoWezla.advertise<geometry_msgs::Twist>("geometry_msgs/Twist", 1);
  subJoy = uchwytDoWezla.subscribe<sensor_msgs::Joy>("joy", 10, &JoyInterpreter::joyCallback, this);
}

/**
 * Callback od odebranej wiadomości. Odebrana wiadomość joy jest typu
 * sensor_msgs/joy. Wiadomość joy przyjmuje wartości rzeczywiste w zakresie
 * [-1, 1]. W funkcji dokonywane jest skalowanie każdej osi poprzez parametr
 * podany w pliku launch. Po odebraniu wiadomośći publicysta publikuje wiadomość
 * typu geometry_msgs/Twist o zadanym kierunku jazdy oraz o zadanej rotacji.
 * @param joy Odebrana wiadomośc
 */
void JoyInterpreter::joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  geometry_msgs::Twist twist;
  twist.angular.z =  maxZ * joy->axes[osRotacjiZ];
  twist.linear.x = maxX * joy->axes[osTranslacjiX];
  twist.linear.y = maxY * joy->axes[osTranslacjiY];
  pubCmdVelocity.publish(twist);
}

int main(int argc, char** argv)
{
  ros::init(argc,argv, "JoyInterpreter");
  JoyInterpreter joystick;

  ros::spin();
}
