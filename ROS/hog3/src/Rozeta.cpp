#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include "../inc/rozeta.hh"

/*!
   \file Rozeta.cpp
   \brief W pliku jest zdefiniowany wezęł odpowiedzialny za sterowanie robota
	 w sposób umożliwiający wykonanie rozety.
   \author Dawid Marszałkiewicz
*/

/**
 * \brief Klasa zadaje sterowanie poprzez wiadomości geometry_msgs/Twist
 */
class Rozeta {
public:
	Rozeta(float A, float B, float H);
	void publish(float t);
private:
	ros::NodeHandle node;
	ros::Publisher pubTwist;
	float a, b, h;
};

Rozeta::Rozeta(float A, float B, float H): a(A), b(B), h(H) {
	pubTwist = node.advertise<geometry_msgs::Twist>("geometry_msgs/Twist", 1);
}

/**
 * Funckja typu Callback. Publikuje wiadomości na podstawie obliczonych sterowań
 * @param t Chwila czasu
 */
void Rozeta::publish(float t) {
	geometry_msgs::Twist msg;

	Vect2D xy;
	float omega;
	rozeta2(xy, omega, t, a, b, h);

	msg.linear.x = xy.x[0];
	msg.linear.y = xy.x[1];
	msg.angular.z = omega;
	pubTwist.publish(msg);
}


/**
 * Funkcja main
 * @param  argc Liczba argumentów
 * @param  argv Parametry epitrochoidy
 */
int main(int argc, char** argv) {
	if (argc != 5)
		std::cerr << "Wywołanie: " << argv[0] << " <a> <b> <h> <skalowanie_czasu>" << std::endl;

	float a, b, h, skalowanie_czasu;
	a = std::stof(std::string(argv[1]));
	b = std::stof(std::string(argv[2]));
	h = std::stof(std::string(argv[3]));
	skalowanie_czasu = std::stof(std::string(argv[4]));


	ros::init(argc, argv, "Rozeta");
	Rozeta rozeta(a, b, h);

	ros::Rate rate(50);
	double t = 0;
	while (ros::ok()) {
		rozeta.publish(t);
		t += 1.0 / 50 * skalowanie_czasu;
		rate.sleep();
	}
}
