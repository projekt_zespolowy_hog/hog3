#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>
#include <iostream>
#include <ros/ros.h>
#include <hog3/ServoAngles.h>
#include <cmath>
#include <iostream>

/*!
   \file RobotLink.cpp
   \brief W pliku znajduje się definicja klasy RobotLinker, który jest łącznikiem
   pomiędzy robotem a ROSem
   \author Dawid Marszałkiewicz
*/

// typedef struct{
//   int W;
//   int A;
//   int B;
// }Naped;

/**
 * \brief Radiany na stopnie
 */
int rad2deg(const int);

/**
 * \brief Klasa modeluje pojęcie RobotLinkera, który wykonuje połączenie z
 * rzeczywistym robotem oraz przekazuje dane do niego po odpowiedniej konwersji
 */
class RobotLinker
{
public:
  /**
   * \brief Konstruktor RobotLinkera
   */
  RobotLinker();
  ~RobotLinker()
  {
    close(bluetoothSocket);
    delete[] bufor;
  }

private:
  /**
   * \brief Uchwyt do stworzonego węzła w ROSie
   */
  ros::NodeHandle uchwytDoWezla;
  /**
   * \brief Subskrybent otrzymujący wiadomości o wychyleniach serw
   */
  ros::Subscriber subAngles;

  int bluetoothSocket, rozmiarBufora;
  int kanal, predkoscKatowaSfer;
  /**
   * \brief Struktura służąca do łączenia z robotem
   */
  struct sockaddr_rc robot;

  /**
   * \brief MAC adres robota
   */
  const char *macRobot = "20:16:05:30:64:57";
  char* bufor;

  /**
   * \brief Callback, który jesty wywoływany przy odebraniu wiadomości
   */
  void odebranoWiadomosc(const hog3::ServoAngles::ConstPtr& katy);
};

/**
 * W konstruktorze dokonuje się dodania parametrów (channel, bufsize, ang_vel),
 * których wartości można zainicjalizować przez plik roslaunch.
 *
 * W kosntruktorze również następuje łączenie z robotem. W wypadku braku
 * połączenia węzeł umiera.
 *
 * \post Ustanowione stabline połączenie z robotem
 */

RobotLinker::RobotLinker(): rozmiarBufora(1), predkoscKatowaSfer(50)
{
  uchwytDoWezla.param("channel", kanal, 1);
  uchwytDoWezla.param("bufsize", rozmiarBufora, rozmiarBufora);
  uchwytDoWezla.param("ang_vel", predkoscKatowaSfer, predkoscKatowaSfer);


  robot = {0};
  bufor = new char[rozmiarBufora];
  bluetoothSocket = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
  robot.rc_family = AF_BLUETOOTH;
  robot.rc_channel = (uint8_t) kanal;
  str2ba(macRobot, &robot.rc_bdaddr);

  int status = connect(bluetoothSocket, (struct sockaddr*) &robot, sizeof(robot));
  if (status !=  0){
    uchwytDoWezla.shutdown();
    exit(2);
  }

  subAngles = uchwytDoWezla.subscribe<hog3::ServoAngles>\
              ("hog3/ServoAngles", 10, &RobotLinker::odebranoWiadomosc, this);
}


/**
 * Funkcja typu Callback wywoływana przy każdorazowym odebraniu wiadomośći przez
 * subAngles. W funkcji następuje zamiania odebrannych katów z radianów na katy
 * oraz ich wysyłanie przez Bluetootha do robota w formacie:
 *
 *\verbatim
  < W LE LI W FE FI W RE RI
 \endverbatim
 * gdzie:
 * - \b W - prędkość kątowa sfery\n
 * - \b Lx - wychylenie platformy lewej\n
 * - \b Fx - wychylenie platfromy przedniej\n
 * - \b Rx - wychylenie platformy prawej\n\n
 * W wyżej wymienionym przypadku x przyjmuje wartości:
 * - \b E (external) zewnętrzna część platformy\n
 * - \b I (internal) wewnęrzna część platformy \n\n
 *
 * Wszystkei wartości kątów są przesunięte o 90 stopni
 * @param katy Odebrane kąty w radianach
 */
void RobotLinker::odebranoWiadomosc(const hog3::ServoAngles::ConstPtr& katy)
{
  uint8_t wiadomosc[9];
  wiadomosc[0] = predkoscKatowaSfer;
  // std::cout << predkoscKatowaSfer << std::endl;
  wiadomosc[1] = katy->left.external*180/M_PI + 90;
  wiadomosc[2] = katy->left.internal*180/M_PI + 90;
  wiadomosc[3] = predkoscKatowaSfer;
  wiadomosc[4] = katy->front.external*180/M_PI + 90;
  wiadomosc[5] = katy->front.internal*180/M_PI + 90;
  wiadomosc[6] = predkoscKatowaSfer;
  wiadomosc[7] = katy->right.external*180/M_PI + 90;
  wiadomosc[8] = katy->right.internal*180/M_PI + 90;

  write(bluetoothSocket, "<", 1);
  write(bluetoothSocket, wiadomosc, 9);
  // write(bluetoothSocket, ">", 1);
}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "RobotLink");
    RobotLinker laczeDoRobota;
    ros::spin();
}

/**
 * Funkcja zamieniająca wartość kąta w radianach na wartość kata w stopniach
 * @param  rad Wartość kąta w radianach
 * @return     Wartość kąta w stopniach
 */
int rad2deg(const int rad)
{
  return rad * 180 / M_PI;
}
