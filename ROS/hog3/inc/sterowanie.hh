/*
  sterowanie do Hoggera
  7 marca: wersja 0.1 - proste sterowanie z prawdopodobnie błędnymi układami współrzędnych
  2 kwietnia: wersja sterowania 0.1 gotowa do zastosowania
 */
#include<iostream>
#include <cmath>


using namespace std;

/*
       PRZOD
         /\
        /  \
       /    \
      /      \
LEWA  ________ PRAWA

   y^
    |
    |
    |
    _______>
           x
 */

const float PROMIEN_SFERY=0.05;
const float MAXR=0.03;//promień na maksymalnym wychyle serwa
typedef float angle;
typedef struct Vect2D{float x[2];} Vect2D;
//typedef struct Vect2D{float x, float y;} Vect2D;

/*
IN:
  v[velocity]: predkosc 2D w ukladzie srodka hoggera [m/s]
  w[omega]: predkosc obrotowa hoggera [rad/s] (dodatnie-obrot w lewo patrzac z gory)
OUT:
  serw: 6 katow ktore nalezy zadac na serwomechanizmy
  [0]: lewy naped, zewnetrzny pierscien
  [1]: lewy naped, wewn
  [2]: przedni   , zewn
  [3]: przedni   , wewn
  [4]: prawy     , zewn
  [5]: prawy     , wewn
retval: 0-powodzenie, inne-kody bledow(?todo)
*/
float len(Vect2D v)
{
  return sqrt(v.x[0]*v.x[0]+v.x[1]*v.x[1]);
}
int A(Vect2D v[3], const Vect2D vin, const angle w, const angle wsfer=100);//domyślnie 100rad/s
int B(angle *a, angle *b , const Vect2D v, const angle w=100);//domyślnie 100rad/s
int C(angle *s1, angle *s2, const angle a, const angle b);
int ABCstep(angle serv[6], const Vect2D v, const angle w, const angle wsfer=100)
{
  Vect2D predkosci_w_napedach[3];
  A(predkosci_w_napedach,v, w, wsfer);
  for(int i=0;i<3;++i)
    {
      angle a,b;
      B(&a,&b,predkosci_w_napedach[i], wsfer);
      C(&serv[2*i],&serv[2*i+1],a,b);
    }
  return 0;
}
/*
IN:
  vin[velocity]: predkosc 2D w ukladzie srodka hoggera [m/s]
  w[omega]: predkosc obrotowa hoggera [rad/s] (dodatnie-obrot w lewo patrzac z gory)
  wsfer[omega sfer]: predkosc obrotowa sfery [rad/s] (dodatnie-obrot w lewo patrzac z gory)
OUT:
  v: predkosci w trzech puktachpodparcia
  [0]: lewy
  [1]: przod
  [2]: prawy
retval: 0-powodzenie, inne-kody bledow(?todo)
 */
int A(Vect2D v[3], const Vect2D vin, const angle w, const angle wsfer)
{
  //liczymy tutaj superpozycje wektora prędkości 2D oraz tych pochodzących od obrotu
#define ODLEGLOSC_NAPEDU_OD_GEOM_SRODKA_HOGGERA_W_METRACH 0.155167 //155.167mm
  const Vect2D
    a={0.5,-sin(M_PI/3)},
    b={ -1,          0 },
    c={0.5, sin(M_PI/3)};

  const float skala=ODLEGLOSC_NAPEDU_OD_GEOM_SRODKA_HOGGERA_W_METRACH;
  float
    c120x=vin.x[0]*(-0.5),
    s120y=vin.x[1]*sin(M_PI/3),
    s120x=vin.x[0]*sin(M_PI/3),
    c120y=vin.x[1]*(-0.5);
  //        ....obrot...+..vlin..
  v[0].x[0]=b.x[0]*skala*w +c120x +s120y;
  v[0].x[1]=b.x[1]*skala*w -s120x +c120y;
  v[1].x[0]=b.x[0]*skala*w+vin.x[0];
  v[1].x[1]=b.x[1]*skala*w+vin.x[1];
  v[2].x[0]=b.x[0]*skala*w +c120x -s120y;
  v[2].x[1]=b.x[1]*skala*w +s120x +c120y;

  //if(0)
  {//ograniczenie na Vmax - bez niego możliwe jest żądanie żeby np asin(x) zwracał wartość dla x spoza [-1,1]
    float Vmax=0.0171010072*wsfer;//omega*3cm - maksymalna możliwa prędkość
    float przeskaluj=1;
    for(int i=0;i<3;++i)
      if(len(v[i])>Vmax*przeskaluj)
	przeskaluj=Vmax/len(v[i]);

    v[0].x[0]*=przeskaluj;//szybkość kosztem rozmiaru programu
    v[0].x[1]*=przeskaluj;
    v[1].x[0]*=przeskaluj;
    v[1].x[1]*=przeskaluj;
    v[2].x[0]*=przeskaluj;
    v[2].x[1]*=przeskaluj;
  }
  return 0;
}
/*
IN:
  v[velocity]: predkosc 2D w ukladzie srodka hoggera [m/s]
  w[omega]: predkosc obrotowa sfery [rad/s] (dodatnie-obrot w lewo patrzac z gory)
  MAXR[maksymalny promien]: maksymalny promien mozliwy do osiagniecia (w teorri promien sfery)
OUT:
  a[alfa]: wychyl w  Zewnetrznym pierscieniu [rad]
  b[beta]: wychyl w  Wewnetrznym pierscieniu [rad]
retval: 0-powodzenie, inne-kody bledow(?todo)
 */
int B(angle *a, angle *b , const Vect2D v, const angle w)
{//1RPM=0,104719755rad/s
  //przód: oś Y
  //obrót 1: oś X (pierścień zewn)
  //obrót 2: oś Y (pierścień wewn)

  //poniższe należy zrealizować przy generacji prędkości
  // if(MAXR*w < speed)//jeśli żądamy szybszego ruchu niż realizowalny
  //   {
  //     const float bound_coef=0.99*MAXR*w/speed;
  //     v.x[0]*= bound_coef;
  //     v.x[1]*= bound_coef;
  //   }

  //beta=asin(-V_y/omega/R);
  //alfa=asin(-V_x/(cos(beta)*omega/R);
  *b=asin(-v.x[1]/(PROMIEN_SFERY*w));
  *a=asin(-v.x[0]/(PROMIEN_SFERY*w*cos(*b)));

  return 0;
}
/*
IN:
  a[alfa]: wychyl w  Zewnetrznym pierscieniu [rad]
  b[beta]: wychyl w  Wewnetrznym pierscieniu [rad]
OUT:
  s1: wychyl serwa podpietego na Zewnetrzny pierscien [jednostka do ustalenia    ]
  s2: wychyl serwa podpietego na Wewnetrzny pierscien [byc moze w ms? (wej serwa)]
retval: 0-powodzenie, inne-kody bledow(?todo)
 */
int C(angle *s1, angle *s2, const angle a, const angle b)
{
  *s1=a;
  *s2=b;
  return 0;
}

/* USAGE:
  angle serv[6];
  Vect2D Vin;
  angle omega_zadana;

  cout<<"podaj kolejno współrzędne wej(x y omega): ";
  cin>> Vin.x[0] >> Vin.x[1] >> omega_zadana;
  ABCstep(serv, Vin, omega_zadana);
*/
