var indexSectionsWithContent =
{
  0: "abcdhjklmnoprsuvx~",
  1: "jmrv",
  2: "hjmrsv",
  3: "abcjlmoprsv~",
  4: "abhkmnoprsuvx",
  5: "av",
  6: "o",
  7: "d"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Klasy",
  2: "Pliki",
  3: "Funkcje",
  4: "Zmienne",
  5: "Definicje typów",
  6: "Definicje",
  7: "Strony"
};

