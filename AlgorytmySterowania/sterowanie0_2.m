%%symulacja sterowania
%%setup
%% x y
%%v=[1,1]%% v zadane
clear all;
close all;
%%1st/s = 

R=0.05 %%w metrach
omega=4 %%w rad/s   1rad/s=10rpm
q=[0,0] %%wsp w radianach na serwach
v=[0,0.1] %%w m/s
dt=1/100;%%kwant czasu w sek
t=1;%%czas symulaji w sek

## v0=[0,0]
## amax=1
## vmax=R*omega
epsilon=0.01;

function r = q2r(q,R)
  r(1)=sin(q(2))*R;
  r(2)=sin(q(1))*cos(q(2))*R;
endfunction
function v = r2v(r,omega)
  v=zeros(size(r));
  v(1)=-r(2)*omega;
  v(2)=r(1)*omega;
endfunction
function v = q2v(q,omega,R)
  v=zeros(size(q));
  v(2)=sin(q(2))*R*omega;
  v(1)=-sin(q(1))*cos(q(2))*R*omega;
endfunction
function q = r2q(r,R)
  q=zeros(size(r));
  q(2)=asin(r(1)/R);
  q(1)=asin(r(2)/cos(q(2))/R);
endfunction
function r = v2r(v,omega)
  r=zeros(size(v));
  r(1)=v(2)/omega;
  r(2)=-v(1)/omega;
endfunction
function bool_val = katDODATNI(a,b)
  %%zwraca czy kąt z wektora a do b jest dodatni
  bool_val=(a(1)*b(2)-a(2)*b(1))>0;%%to wynika z iloczynu wektorowego
endfunction
%%ster 0.1: (p.B)
function q = v2q01(v,omega,R)
  q=zeros(size(v));
  q(2)=asin(v(2)/omega/R);
  q(1)=asin(-v(1)/cos(q(2))/omega/R);
endfunction

%%ster 0.2: (p.B)
function dq = v2q02(q,v,omega,R)
  dq=zeros(size(q));;
  dq(1)=-(v(2)-sin(q(2))*omega*R)/R;
  dq(2)=-(v(1)+sin(q(1))*cos(q(2))*omega*R)/(cos(q(1))*R);
endfunction

function b=saturate(a,val)
  b=zeros(size(a));
  val=abs(val);
  for i=1:length(a)
    if a(i)>val
      b(i)=val
      disp('sat zadzialal')
    elseif a(i)<-val
      b(i)=-val
      disp('sat zadzialal')
    else
      b(i)=a(i);
    end
  end
endfunction

sk2=100;
hold on;
r=q2r(q,R);
s=[0,0];

vpop=[0.02,0]
q=v2q01(vpop,omega,R);
vk=[0,.1]

alfa=-0.5*(vpop(1)^2-vk(1)^2+vk(2)^2-vpop(2)^2)/(vk(1)*(vpop(1)-vk(1))-vk(2)*(vk(2)-vpop(2)));
vmid=vk*alfa;
%%v=randn(1,2)
for i=1:(t/dt)
  figure 1
  if(mod(i,50)==0)
    %v=2*randn(1,2)
  end

  

  if i>0 %% przejedź troche
    ##dq=saturate(v2q02(q,v0,omega,R),dt*2*pi);
    if(norm(q2v(q,omega,R) - vk)>epsilon)
      %%if(q2v(q,omega,R)==0 || katDODATNI(q2v(q,omega,R),v0))
      %%vmid=vk-(norm(vk)+norm(vpop))/2*vk/norm(vk);
      
      dq=v2q02(q,vmid,omega,R);
      q+=dt*dq;
    else
      q=v2q01(vk,omega,R);
    end
    ## q=saturate(q,0.9*pi/2);

    ## q+=saturate(dq,dt*2*pi);
    ## q=saturate(q,0.9*pi/2);
    
    %%skala*q+(1-skala)*v2q01(v,omega,R);
  end
  
  vpop=q2v(q,omega,R);
  dr=r;
  r=q2r(q,R);
  dr=(r-dr)/dt;
  
  vv=r2v(r,omega);
  plot(sk2*r(1),sk2*r(2),'b');
  plot(vv(1),vv(2),'r');%%v od napędu
  plot(-dr(1),-dr(2),'g');%%v od serw
  plot([0,vv(1)],[0,vv(2)],'r');%%linia czerwona - skladowa napedu od sfer
  plot([vv(1),vv(1)-dr(1)],[vv(2), vv(2)-dr(2)],'g');%%linia zielona - skladowa predkosci od serw
  plot(vv(1)-dr(1),vv(2)-dr(2),'m');%%wypadkowa predkosc
  plot(i*dt/1,i*dt/1,'y');%%czas - sluzy jako pomoc w obserwacji
  pause(dt); %% "animacja"
  
  figure 2
  hold on
  s+=(vv-dr)*dt;
  plot(s(1),s(2));%% przebyta droga
end

%{
for i=1:(t/dt)

  r=v2r(v,omega);
  q=r2q(r,R);

  
  vv=r2v(r,omega);
  plot(sk2*r(1),sk2*r(2),'b');
  plot(vv(1),vv(2),'r');%%v od napędu
  plot(-dr(1),-dr(2),'g');%%v od serw
  plot([vv(1),vv(1)-dr(1)],[vv(2), vv(2)-dr(2)],'m');
  pause(dt);
end
%}
