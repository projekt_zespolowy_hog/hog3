#include<cmath>
typedef float angle;
typedef struct Vect2D{float x[2];} Vect2D;

/*!
  \brief funkcja realizująca trajektorię Epitrochoidy
  https://pl.wikipedia.org/wiki/Epitrochoida
  \param czas [in] aktualna chwila czasu w symulacji
  \param R [in] promień dużego koła Epitrochoidy 
  \param r [in] promień małego koła Epitrochoidy 
  \param d [in] odległość punktu środka robota od środka mniejszego koła Epitrochoidy 
*/
void rozeta(Vect2D &v_robota, angle &omega_robota, float czas, float R, float r, float d)
{
  //https://pl.wikipedia.org/wiki/Epitrochoida
  /*!
    \brief promień dużego koła
  */
  float a=R;
  /*!
    \brief promień małego koła
  */
  float b=r;
  /*!
    \brief odległość punktu środka robota od środka mniejszego koła
  */
  float h=d;
  /*!
    \brief promień dużego koła
  */
  float t=czas;
  //http://www.wolframalpha.com/input/?i=Epitrochoid
  //http://www.wolframalpha.com/input/?i=d%2Fdt+(a%2Bb)cos(t)+-+h+cos(t*(a%2Bb)%2Fb)
  v_robota.x[0]= h*(a+b)*sin(t*(a+b)/b)/b - (a+b)*sin(t);
  //http://www.wolframalpha.com/input/?i=d%2Fdt+(a%2Bb)sin(t)+-+h+sin(t*(a%2Bb)%2Fb)
  v_robota.x[1]= -h*(a+b)*cos(t*(a+b)/b)/b + (a+b)*cos(t);
}
