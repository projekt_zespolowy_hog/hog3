# Hog3

Projekt robota opartego na napędzie typu HOG *(Hemispherical Omnidirectional Gimbaled drive)* wykorzystującego 3 półsfery.

Sterowanie robotem możliwe jest za pomocą joysticka i oparte jest o system ROS. 
ROS uruchamiany jest w kontenerze Dockera, dzięki czemu jest (teoretycznie) niezależny od systemu operacyjnego.
Symulacja działania robota możliwa jest w programie V-REP. Niestety na razie symulator nie działa wewnątrz kontenera.

## Uruchamianie
Instrukcja pisana z myślą o linuxie, ale może jakimś cudem zadziała na Windowsie.

---

**Instalacja programów**

- zainstalować  *Docker  CE* (Community Edition):
	- na [stronie](https://docs.docker.com/engine/installation/#supported-platforms) wybrać odpowiedni system i postępować zgodnie z instrukcją
	- warto potem wykonać [parę czynności](https://docs.docker.com/engine/installation/linux/linux-postinstall/), żeby nie musieć ciągle korzystać z *sudo*, i żeby *docker* startował wraz z systemem
	
- zainstalować [docker-compose](https://docs.docker.com/compose/install/) zgodnie z instrukcją

- pobrać program [V-REP PRO EDU](http://www.coppeliarobotics.com/downloads.html) w wersji 3.4:
	- rozpakować paczkę i przenieść katalog V-REPa dokąd chcemy
	- wejść do katalogu V-REPa i wykonać:
~~~bash
cp compiledRosPlugins/libv_repExtRosInterface.so .
~~~

---

**Uruchamianie systemu**

W  katalogu projektu *hog3*:

- uruchomić skrypt konfigurujący sieć oraz IP dla ROSa na kontenerze:
~~~bash
./setup.sh
~~~

- zbudować całość systemu:
~~~bash
docker-compose build
~~~

- **(!)** upewnić się, że joystick jest podłączony i zamontowany pod ścieżką */dev/input/js0* (jeżeli ma być bez joysticka to trzeba zakomentować odpowiedni fragment *docker-compose.yml*)

- uruchomić system poleceniem
~~~bash
docker-compose up
~~~ 

> w przypadku, gdy brak połączenia bluetooth z robotem  węzeł *RobotLink* może umrzeć, nie należy się tym przejmować ;)

---
- w tym momencie cały ROS powinien już działać wewnątrz dockera,
aby to sprawdzić można wejśc do kontenera poleceniem *docker exec -it hog3_ros bash* i wywoływać polecenia ROSa

- aby połączyć się z kontenerem z lokalnej maszyny należy skorzystać ze skryptu konfigurującego powłokę
~~~bash
> ./runbash.sh
~~~
sprawdzenie działania:
~~~bash
> rostopic list
...
> rostopic echo /hog3/Twist
...
~~~

- w powłoce połączonej z kontenerem uruchomić program V-REP

- *"Koniec i bomba, a kto czytał ten trąba!"*