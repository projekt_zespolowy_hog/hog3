## Gęstości materiałów [kg/m^3^]

- **dystanse:** 7800 (stal)

- **podstawy:** 1180 (plexi)

- **montaże:** 720 (PLA 60%)

- **serwa:** 1770 (60g)

- **BLDC:** 1800 (50g)

- **sfery:** (na razie) 2000

---

**Waga całości:** ~ 1.99 kg

## Joints

* **Serwa:**	
	- **Specyfikacja i założenia:**
		- napięcie zasilania: 5V
		- praca dla 4,8V:
			- Moment: 15,5 kg*cm (1,52 Nm)
			- Prędkość: 0,16 s/(60 stopni)
	- **W symulacji:**
		- zakres ruchu: (-20, 20) stopni
		- step size: 0.5 stopnia
		- maksymalny moment: 1,5 Nm
		- upper velocity limit: 375 stopni/s
		- TODO: dostroić PIDa

* **BLDC:**	
	- **Specyfikacja i założenia:**
		- napięcie zasilania: 11.1V
		- prędkość obrotowa: 950 RPM/V, tj:
			- 10 450 RPM
			- ~ 174 RPs
			- 62 700 stopni/s
			- ~ 1094 rad/s
		- moc: 100 W
	- **W symulacji:**
		- TODO:
		- target vel: 1000 RPM
		- max moment: P / omega
			- omega = 1000 RPM -> T = 0.955

---

**Pozycja modelu:**
Pozycja w *inventor2ros.ttt* jest ustawiona tak, aby importować siatki z Inventora od razu w odpowiednie miejsce. 
Przed zapisaniem modelu ustawić taką pozycję:

- X = 0
- Y = 0
- Z = 6.05e-2 [m]
