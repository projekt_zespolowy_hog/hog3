#! /bin/bash
# creates network for docker system
# finds gateway ip adress and preperes .env file for docker-compose

# usage ./setup.sh [rm]

network="hog3_network"

### ./setup.sh rm
# remove network
if (( $# > 0 )) && [[ $1 = 'rm' ]]; then
	if docker network ls | grep -q $network; then
		echo 'Removing network:'
		docker network rm $network
	else
		echo "Network $network doesn't exist"
	fi
	exit 0
fi


### ./setup.sh
# create network
if ! docker network ls | grep -q $network; then
	echo 'Creating network:'
	docker network create $network
fi

# get gateway ip adress
HOST_ROS_IP=$(docker network inspect --format='{{range .IPAM.Config}}{{.Gateway}}{{end}}' $network)

# generate new ip adress for ros container
new_last_digit=2
DOCKER_ROS_IP=$(echo $HOST_ROS_IP | sed -r "s/([0-9]+\.[0-9]+\.[0-9]+\.)[0-9]+/\1$new_last_digit/g")

# create env file for compose
echo "ROS_IP=$DOCKER_ROS_IP" > .env
