var NAVTREE =
[
  [ "Hogger 3", "index.html", [
    [ "Dokumentacja oprogramowania Hogger 3", "index.html", null ],
    [ "Klasy", "annotated.html", [
      [ "Lista klas", "annotated.html", "annotated_dup" ],
      [ "Indeks klas", "classes.html", null ],
      [ "Składowe klas", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Funkcje", "functions_func.html", null ],
        [ "Zmienne", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Pliki", null, [
      [ "Lista plików", "files.html", "files" ],
      [ "Składowe plików", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Funkcje", "globals_func.html", null ],
        [ "Zmienne", "globals_vars.html", null ],
        [ "Definicje typów", "globals_type.html", null ],
        [ "Definicje", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_joy_interpreter_8cpp.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';