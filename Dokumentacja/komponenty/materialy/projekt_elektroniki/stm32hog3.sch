EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:arduino_shieldsNCL
LIBS:stm32hog3-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X02 P1
U 1 1 5927FEF2
P 3330 2170
F 0 "P1" H 3330 2320 50  0000 C CNN
F 1 "POWER" V 3430 2170 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x01_Pitch2.54mm" H 3330 2170 50  0001 C CNN
F 3 "" H 3330 2170 50  0000 C CNN
	1    3330 2170
	-1   0    0    1   
$EndComp
$Comp
L SWITCH_INV SW1
U 1 1 5927FF58
P 4030 2120
F 0 "SW1" H 3830 2270 50  0000 C CNN
F 1 "SWITCH_ON" H 3880 1970 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_Micro_SPST_Angled" H 4030 2120 50  0001 C CNN
F 3 "" H 4030 2120 50  0000 C CNN
	1    4030 2120
	1    0    0    1   
$EndComp
$Comp
L GND #PWR01
U 1 1 5927FF9D
P 3530 2220
F 0 "#PWR01" H 3530 1970 50  0001 C CNN
F 1 "GND" H 3530 2070 50  0000 C CNN
F 2 "" H 3530 2220 50  0000 C CNN
F 3 "" H 3530 2220 50  0000 C CNN
	1    3530 2220
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR02
U 1 1 5927FFD5
P 4530 2020
F 0 "#PWR02" H 4530 1870 50  0001 C CNN
F 1 "VCC" H 4530 2170 50  0000 C CNN
F 2 "" H 4530 2020 50  0000 C CNN
F 3 "" H 4530 2020 50  0000 C CNN
	1    4530 2020
	1    0    0    -1  
$EndComp
NoConn ~ 4530 2220
$Comp
L CONN_01X03 P3
U 1 1 592800CE
P 6345 2485
F 0 "P3" H 6345 2685 50  0000 C CNN
F 1 "CONN_01X03" V 6445 2485 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 6345 2485 50  0001 C CNN
F 3 "" H 6345 2485 50  0000 C CNN
	1    6345 2485
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG03
U 1 1 5928028C
P 4115 1325
F 0 "#FLG03" H 4115 1420 50  0001 C CNN
F 1 "PWR_FLAG" H 4115 1505 50  0000 C CNN
F 2 "" H 4115 1325 50  0000 C CNN
F 3 "" H 4115 1325 50  0000 C CNN
	1    4115 1325
	-1   0    0    1   
$EndComp
$Comp
L +5V #PWR04
U 1 1 592802CF
P 3790 1295
F 0 "#PWR04" H 3790 1145 50  0001 C CNN
F 1 "+5V" H 3790 1435 50  0000 C CNN
F 2 "" H 3790 1295 50  0000 C CNN
F 3 "" H 3790 1295 50  0000 C CNN
	1    3790 1295
	1    0    0    -1  
$EndComp
Text Label 3895 1310 0    60   ~ 0
3V3
$Comp
L PWR_FLAG #FLG05
U 1 1 59280458
P 4465 1315
F 0 "#FLG05" H 4465 1410 50  0001 C CNN
F 1 "PWR_FLAG" H 4465 1495 50  0000 C CNN
F 2 "" H 4465 1315 50  0000 C CNN
F 3 "" H 4465 1315 50  0000 C CNN
	1    4465 1315
	-1   0    0    -1  
$EndComp
Text Label 4245 1330 0    60   ~ 0
GND
Text Label 6545 2385 0    60   ~ 0
PWM_A1
Text Label 6545 2485 0    60   ~ 0
5V
$Comp
L PWR_FLAG #FLG06
U 1 1 592805F4
P 3440 1325
F 0 "#FLG06" H 3440 1420 50  0001 C CNN
F 1 "PWR_FLAG" H 3440 1505 50  0000 C CNN
F 2 "" H 3440 1325 50  0000 C CNN
F 3 "" H 3440 1325 50  0000 C CNN
	1    3440 1325
	-1   0    0    1   
$EndComp
Text Label 3220 1310 0    60   ~ 0
VCC
$Comp
L PWR_FLAG #FLG07
U 1 1 59280604
P 3790 1325
F 0 "#FLG07" H 3790 1420 50  0001 C CNN
F 1 "PWR_FLAG" H 3790 1505 50  0000 C CNN
F 2 "" H 3790 1325 50  0000 C CNN
F 3 "" H 3790 1325 50  0000 C CNN
	1    3790 1325
	-1   0    0    1   
$EndComp
Text Label 3570 1310 0    60   ~ 0
5V
$Comp
L +3.3V #PWR08
U 1 1 5928060E
P 4115 1295
F 0 "#PWR08" H 4115 1145 50  0001 C CNN
F 1 "+3.3V" H 4115 1435 50  0000 C CNN
F 2 "" H 4115 1295 50  0000 C CNN
F 3 "" H 4115 1295 50  0000 C CNN
	1    4115 1295
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR09
U 1 1 59280660
P 3440 1295
F 0 "#PWR09" H 3440 1145 50  0001 C CNN
F 1 "VCC" H 3440 1445 50  0000 C CNN
F 2 "" H 3440 1295 50  0000 C CNN
F 3 "" H 3440 1295 50  0000 C CNN
	1    3440 1295
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 592806FA
P 4465 1350
F 0 "#PWR010" H 4465 1100 50  0001 C CNN
F 1 "GND" H 4465 1200 50  0000 C CNN
F 2 "" H 4465 1350 50  0000 C CNN
F 3 "" H 4465 1350 50  0000 C CNN
	1    4465 1350
	1    0    0    -1  
$EndComp
Text Label 6545 2585 0    60   ~ 0
GND
$Comp
L CONN_01X03 P4
U 1 1 59280A45
P 6345 3125
F 0 "P4" H 6345 3325 50  0000 C CNN
F 1 "CONN_01X03" V 6445 3125 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 6345 3125 50  0001 C CNN
F 3 "" H 6345 3125 50  0000 C CNN
	1    6345 3125
	-1   0    0    1   
$EndComp
Text Label 6545 3025 0    60   ~ 0
PWM_B1
Text Label 6545 3125 0    60   ~ 0
5V
Text Label 6545 3225 0    60   ~ 0
GND
Text Label 6545 3715 0    60   ~ 0
PWM_A2
Text Label 6545 4355 0    60   ~ 0
PWM_B2
Text Label 6545 4455 0    60   ~ 0
5V
$Comp
L CONN_01X03 P5
U 1 1 59280BD7
P 6345 3815
F 0 "P5" H 6345 4015 50  0000 C CNN
F 1 "CONN_01X03" V 6445 3815 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 6345 3815 50  0001 C CNN
F 3 "" H 6345 3815 50  0000 C CNN
	1    6345 3815
	-1   0    0    1   
$EndComp
Text Label 6545 3815 0    60   ~ 0
5V
Text Label 6545 3915 0    60   ~ 0
GND
$Comp
L CONN_01X03 P6
U 1 1 59280BE0
P 6345 4455
F 0 "P6" H 6345 4655 50  0000 C CNN
F 1 "CONN_01X03" V 6445 4455 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 6345 4455 50  0001 C CNN
F 3 "" H 6345 4455 50  0000 C CNN
	1    6345 4455
	-1   0    0    1   
$EndComp
Text Label 6545 4555 0    60   ~ 0
GND
$Comp
L CONN_01X03 P8
U 1 1 59280BE9
P 6345 5195
F 0 "P8" H 6345 5395 50  0000 C CNN
F 1 "CONN_01X03" V 6445 5195 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 6345 5195 50  0001 C CNN
F 3 "" H 6345 5195 50  0000 C CNN
	1    6345 5195
	-1   0    0    1   
$EndComp
Text Label 6545 5095 0    60   ~ 0
PWM_A3
Text Label 6545 5195 0    60   ~ 0
5V
Text Label 6545 5295 0    60   ~ 0
GND
$Comp
L CONN_01X03 P9
U 1 1 59280BF2
P 6345 5835
F 0 "P9" H 6345 6035 50  0000 C CNN
F 1 "CONN_01X03" V 6445 5835 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 6345 5835 50  0001 C CNN
F 3 "" H 6345 5835 50  0000 C CNN
	1    6345 5835
	-1   0    0    1   
$EndComp
Text Label 6545 5735 0    60   ~ 0
PWM_B3
Text Label 6545 5835 0    60   ~ 0
5V
Text Label 6545 5935 0    60   ~ 0
GND
$Comp
L STM32F401RE-NUCLEO SHIELD1
U 1 1 59283084
P 3200 4190
F 0 "SHIELD1" H 2850 5340 60  0000 C CNN
F 1 "STM32F401RE-NUCLEO" H 3250 3240 60  0000 C CNN
F 2 "" H 3200 4190 60  0001 C CNN
F 3 "" H 3200 4190 60  0001 C CNN
	1    3200 4190
	1    0    0    -1  
$EndComp
Text Label 4150 4890 0    60   ~ 0
RX_USB
Text Label 4150 4990 0    60   ~ 0
TX_USB
Text Label 4150 4790 0    60   ~ 0
TX_BT
Text Label 4150 4090 0    60   ~ 0
RX_BT
$Comp
L CONN_01X06 P10
U 1 1 5928385D
P 3190 5805
F 0 "P10" H 3190 6155 50  0000 C CNN
F 1 "CONN_01X06" V 3290 5805 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 3190 5805 50  0001 C CNN
F 3 "" H 3190 5805 50  0000 C CNN
	1    3190 5805
	-1   0    0    -1  
$EndComp
Text Label 3390 5555 0    60   ~ 0
KEY
Text Label 3390 5655 0    60   ~ 0
5V
Text Label 3390 5755 0    60   ~ 0
GND
Text Label 3390 5855 0    60   ~ 0
TX_BT
Text Label 3390 5955 0    60   ~ 0
RX_BT
Text Label 3390 6055 0    60   ~ 0
STATE
NoConn ~ 3390 5555
NoConn ~ 3390 6055
NoConn ~ 4150 4890
NoConn ~ 4150 4990
Text Label 2250 4490 2    60   ~ 0
PWM_A1
Text Label 2250 4590 2    60   ~ 0
PWM_B1
Text Label 4150 3690 0    60   ~ 0
PWM_A2
Text Label 4150 3790 0    60   ~ 0
PWM_B2
Text Label 4150 3190 0    60   ~ 0
PWM_A3
Text Label 4150 3290 0    60   ~ 0
PWM_B3
Text Label 4150 4290 0    60   ~ 0
PWM_W
$Comp
L SBEC S1
U 1 1 59285B89
P 6475 1515
F 0 "S1" H 6575 1915 50  0000 C CNN
F 1 "Stablizator (SBEC)" H 6475 1115 50  0000 C CNN
F 2 "" H 6475 1515 50  0001 C CNN
F 3 "" H 6475 1515 50  0000 C CNN
	1    6475 1515
	1    0    0    -1  
$EndComp
Text Label 5775 1265 2    60   ~ 0
VCC
Text Label 5775 1765 2    60   ~ 0
GND
Text Label 7175 1265 0    60   ~ 0
5V
Text Label 7175 1765 0    60   ~ 0
GND
$Comp
L Regulator R1
U 1 1 59288385
P 9282 1591
F 0 "R1" H 9382 1991 50  0000 C CNN
F 1 "Regulator (ESC/BEC)" H 9282 1191 50  0000 C CNN
F 2 "" H 9282 1591 50  0001 C CNN
F 3 "" H 9282 1591 50  0000 C CNN
	1    9282 1591
	1    0    0    -1  
$EndComp
Text Label 8582 1341 2    60   ~ 0
VCC
Text Label 8582 1841 2    60   ~ 0
GND
NoConn ~ 9980 1341
Text Label 9980 1441 0    60   ~ 0
PWM_W
Text Label 9980 1543 0    60   ~ 0
GND
Text Label 9982 1641 0    60   ~ 0
M1_U
Text Label 9982 1741 0    60   ~ 0
M1_V
Text Label 9982 1841 0    60   ~ 0
M1_W
Text Label 2250 4290 2    60   ~ 0
VCC
NoConn ~ 2250 3590
NoConn ~ 2250 3690
NoConn ~ 2250 3790
NoConn ~ 2250 3890
NoConn ~ 2250 3990
Text Label 2250 4090 2    60   ~ 0
GND
Text Label 2250 4190 2    60   ~ 0
GND
NoConn ~ 2250 4690
NoConn ~ 2250 4790
NoConn ~ 2250 4890
NoConn ~ 2250 4990
NoConn ~ 4150 3390
Text Label 4150 3490 0    60   ~ 0
GND
NoConn ~ 4150 3590
NoConn ~ 4150 3890
NoConn ~ 4150 3990
NoConn ~ 4150 4390
NoConn ~ 4150 4490
NoConn ~ 4150 4690
NoConn ~ 4150 4590
$Comp
L CONN_01X03 P2
U 1 1 5928F71C
P 9165 2510
F 0 "P2" H 9165 2710 50  0000 C CNN
F 1 "CONN_01X03" V 9265 2510 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 9165 2510 50  0001 C CNN
F 3 "" H 9165 2510 50  0000 C CNN
	1    9165 2510
	-1   0    0    -1  
$EndComp
Text Label 9365 2410 0    60   ~ 0
M1_U
Text Label 9365 2510 0    60   ~ 0
M1_V
Text Label 9365 2610 0    60   ~ 0
M1_W
$Comp
L Regulator R2
U 1 1 592907C9
P 9297 3321
F 0 "R2" H 9397 3721 50  0000 C CNN
F 1 "Regulator (ESC/BEC)" H 9297 2921 50  0000 C CNN
F 2 "" H 9297 3321 50  0001 C CNN
F 3 "" H 9297 3321 50  0000 C CNN
	1    9297 3321
	1    0    0    -1  
$EndComp
Text Label 8597 3071 2    60   ~ 0
VCC
Text Label 8597 3571 2    60   ~ 0
GND
NoConn ~ 9995 3071
Text Label 9995 3171 0    60   ~ 0
PWM_W
Text Label 9995 3273 0    60   ~ 0
GND
Text Label 9997 3371 0    60   ~ 0
M2_U
Text Label 9997 3471 0    60   ~ 0
M2_V
Text Label 9997 3571 0    60   ~ 0
M2_W
$Comp
L CONN_01X03 P7
U 1 1 592907D7
P 9180 4240
F 0 "P7" H 9180 4440 50  0000 C CNN
F 1 "CONN_01X03" V 9280 4240 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 9180 4240 50  0001 C CNN
F 3 "" H 9180 4240 50  0000 C CNN
	1    9180 4240
	-1   0    0    -1  
$EndComp
Text Label 9380 4140 0    60   ~ 0
M2_U
Text Label 9380 4240 0    60   ~ 0
M2_V
Text Label 9380 4340 0    60   ~ 0
M2_W
$Comp
L Regulator R3
U 1 1 592909D6
P 9302 4991
F 0 "R3" H 9402 5391 50  0000 C CNN
F 1 "Regulator (ESC/BEC)" H 9302 4591 50  0000 C CNN
F 2 "" H 9302 4991 50  0001 C CNN
F 3 "" H 9302 4991 50  0000 C CNN
	1    9302 4991
	1    0    0    -1  
$EndComp
Text Label 8602 4741 2    60   ~ 0
VCC
Text Label 8602 5241 2    60   ~ 0
GND
NoConn ~ 10000 4741
Text Label 10000 4841 0    60   ~ 0
PWM_W
Text Label 10000 4943 0    60   ~ 0
GND
Text Label 10002 5041 0    60   ~ 0
M3_U
Text Label 10002 5141 0    60   ~ 0
M3_V
Text Label 10002 5241 0    60   ~ 0
M3_W
$Comp
L CONN_01X03 P11
U 1 1 592909E4
P 9185 5910
F 0 "P11" H 9185 6110 50  0000 C CNN
F 1 "CONN_01X03" V 9285 5910 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 9185 5910 50  0001 C CNN
F 3 "" H 9185 5910 50  0000 C CNN
	1    9185 5910
	-1   0    0    -1  
$EndComp
Text Label 9385 5810 0    60   ~ 0
M3_U
Text Label 9385 5910 0    60   ~ 0
M3_V
Text Label 9385 6010 0    60   ~ 0
M3_W
Text Notes 8180 995  0    60   ~ 12
Sekcja sterowania silnikami BLDC
Wire Wire Line
	4115 1295 4115 1325
Wire Wire Line
	3895 1310 4115 1310
Connection ~ 4115 1310
Wire Wire Line
	3440 1295 3440 1325
Wire Wire Line
	3220 1310 3440 1310
Connection ~ 3440 1310
Wire Wire Line
	3790 1295 3790 1325
Wire Wire Line
	3570 1310 3790 1310
Connection ~ 3790 1310
Wire Wire Line
	4465 1315 4465 1350
Wire Wire Line
	4245 1330 4465 1330
Connection ~ 4465 1330
Wire Notes Line
	8125 6290 8125 855 
Wire Notes Line
	8125 855  10570 855 
Wire Notes Line
	10570 855  10570 6290
Wire Notes Line
	10570 6290 8125 6290
Wire Notes Line
	5490 855  5490 6290
Wire Notes Line
	5490 6290 7500 6290
Wire Notes Line
	7500 6290 7500 855 
Text Notes 5550 1000 0    60   ~ 12
Sekcja sterowania serwomechanizmami
Wire Notes Line
	7500 855  5490 855 
Wire Notes Line
	2860 855  2860 2550
Wire Notes Line
	2860 2550 4930 2550
Wire Notes Line
	4930 2550 4930 855 
Wire Notes Line
	4930 855  2860 855 
Text Notes 2925 1000 0    60   ~ 12
Sekcja zasilania
Wire Notes Line
	1605 2805 4945 2805
Wire Notes Line
	4945 2805 4945 6340
Wire Notes Line
	4945 6340 1605 6340
Wire Notes Line
	1605 6340 1605 2805
Text Notes 1675 2940 0    60   ~ 12
Sekcja sterowania i komunikacji
$Comp
L R R5
U 1 1 592A0E95
P 2020 1940
F 0 "R5" V 2100 1940 50  0000 C CNN
F 1 "33k" V 2020 1940 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 1950 1940 50  0001 C CNN
F 3 "" H 2020 1940 50  0000 C CNN
	1    2020 1940
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 592A1143
P 2020 1505
F 0 "R4" V 2100 1505 50  0000 C CNN
F 1 "75k" V 2020 1505 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 1950 1505 50  0001 C CNN
F 3 "" H 2020 1505 50  0000 C CNN
	1    2020 1505
	1    0    0    -1  
$EndComp
Wire Wire Line
	2020 1655 2020 1790
Text Label 1915 1720 2    60   ~ 0
ADC
Wire Wire Line
	1915 1720 2020 1720
Connection ~ 2020 1720
$Comp
L VCC #PWR011
U 1 1 592A20A9
P 2020 1355
F 0 "#PWR011" H 2020 1205 50  0001 C CNN
F 1 "VCC" H 2020 1505 50  0000 C CNN
F 2 "" H 2020 1355 50  0000 C CNN
F 3 "" H 2020 1355 50  0000 C CNN
	1    2020 1355
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 592A25E6
P 2020 2090
F 0 "#PWR012" H 2020 1840 50  0001 C CNN
F 1 "GND" H 2020 1940 50  0000 C CNN
F 2 "" H 2020 2090 50  0000 C CNN
F 3 "" H 2020 2090 50  0000 C CNN
	1    2020 2090
	1    0    0    -1  
$EndComp
Wire Notes Line
	1605 990  2430 990 
Wire Notes Line
	2430 990  2430 2315
Wire Notes Line
	2430 2315 1605 2315
Wire Notes Line
	1605 2315 1605 990 
Text Notes 1655 1110 0    60   ~ 12
Pomiar napięcia
$EndSCHEMATC
